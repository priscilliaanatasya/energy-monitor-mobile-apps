import 'package:flutter/material.dart';

class Default {
  static TextStyle welcomeText(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 30,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle loginText(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 20,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle loginButton(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          color: Colors.white,
        );
  }

  static TextStyle loginDetails(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          color: Colors.blue,
          fontSize: 15,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.underline,
        );
  }

  static TextStyle forgetPass(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          color: Colors.blue,
          fontWeight: FontWeight.bold,
          fontSize: 18,
        );
  }

  static TextStyle forgetDetails(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          color: Colors.black,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle getData(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontWeight: FontWeight.bold,
          fontSize: 18,
          color: Colors.grey,
        );
  }

  static TextStyle device(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontWeight: FontWeight.bold,
          fontSize: 15,
          color: Colors.grey,
        );
  }

  static TextStyle dBoard(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 20,
          color: Colors.grey,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle warning(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 15,
          color: Colors.yellow,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle info(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 13,
          color: Colors.black,
        );
  }

  static TextStyle infoTitle(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Color(0xff151965),
        );
  }

  static TextStyle deviceName(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        );
  }

  static TextStyle addFeedback(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 15,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle editProfile(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 18,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle comparison(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 13,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle comparison2(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: Colors.indigo,
        );
  }

  static TextStyle comparisonTable(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        );
  }

  static TextStyle chartsInfo(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(
          color: Colors.black,
          fontSize: 12,
          fontWeight: FontWeight.bold,
        );
  }
}
