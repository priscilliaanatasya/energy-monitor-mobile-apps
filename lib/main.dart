import 'package:energy_monitor/views/splashScreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MaterialApp(
  theme: ThemeData(
    brightness: Brightness.light,
  ),
  debugShowCheckedModeBanner: false,
  home: SplashScreen(),
));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Emonito',
      debugShowCheckedModeBanner: false,
    );
  }
}
