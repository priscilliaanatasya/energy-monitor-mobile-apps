import 'package:energy_monitor/views/comparison/comparison.dart';
import 'package:energy_monitor/views/device/device.dart';
import 'package:energy_monitor/views/feedback/feedback.dart';
import 'package:energy_monitor/views/homepage.dart';
import 'package:energy_monitor/views/information/info.dart';
import 'package:energy_monitor/views/profile/profile.dart';
import 'package:energy_monitor/views/report.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'login.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  var imageUser = Image.asset('images/final.png');

  //save account usename dan name
  String accountUsername = "";
  String accountName = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
        accountName = pref.getString("name");
      });
    } else {
      setState(() {
        accountUsername = "";
        accountName = "";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _userData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  //logout
  Future _logout() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("isLogin", false);
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => new Login()),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      GestureDetector(
          child: UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Color(0xff0049B8)),
              accountName: Text(accountName),
              accountEmail: Text(accountUsername),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                child: imageUser,
              )),
          onTap: () {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => Profile()),
              (Route<dynamic> route) => false,
            );
          }),
      Expanded(
          child: ListView(children: <Widget>[
        ListTile(
            dense: true,
            leading: Icon(Icons.dashboard),
            title: Text("Dashboard"),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
                (Route<dynamic> route) => false,
              );
            }),
        ListTile(
            dense: true,
            leading: Icon(Icons.important_devices),
            title: Text("Device"),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => Device()),
                (Route<dynamic> route) => false,
              );
            }),
        ListTile(
            dense: true,
            leading: Icon(Icons.data_usage),
            title: Text("Report"),
            onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Report()),
                  (Route<dynamic> route) => false,
                )),
        ListTile(
            dense: true,
            leading: Icon(Icons.insert_chart),
            title: Text("Comparison"),
            onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Comp()),
                  (Route<dynamic> route) => false,
                )),
        ListTile(
            dense: true,
            leading: Icon(Icons.feedback),
            title: Text("Feedback"),
            onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => FeedbackEmonito()),
                  (Route<dynamic> route) => false,
                )),
        ListTile(
            dense: true,
            leading: Icon(Icons.info),
            title: Text("Information"),
            onTap: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Info()),
                  (Route<dynamic> route) => false,
                )),
        ListTile(
            dense: true,
            leading: Icon(Icons.exit_to_app),
            title: Text("Log Out",
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                )),
            onTap: () {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        backgroundColor: Color(0xff84142d),
                        title: Text(
                          "Are you sure want to logout?",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        actions: <Widget>[
                          Row(children: <Widget>[
                            FlatButton(
                              onPressed: () {
                                _logout();
                              },
                              child: const Text(
                                'Yes',
                                style: TextStyle(color: Colors.yellow),
                              ),
                            ),
                            FlatButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('No',
                                    style: TextStyle(
                                      color: Colors.grey,
                                    )))
                          ])
                        ]);
                  });
            })
      ]))
    ]);
  }
}
