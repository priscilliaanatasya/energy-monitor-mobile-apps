import 'dart:async';
import 'dart:convert';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/appDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;
import 'package:shared_preferences/shared_preferences.dart';

class Dboard extends StatefulWidget {
  @override
  _DboardState createState() => _DboardState();
}

class _DboardState extends State<Dboard> {
  var dateNow = new DateFormat('EEEE, d MMM, yyyy').format(new DateTime.now());
  var power = true;
  var voltage = true;
  Timer timer;
  var x = 0;
  var powerdata;

  List monthly;
  List awal;
  List akhir;
  List daily;

  monthlyRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-monthly",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        monthly = json.decode(response.body);
      });
    }
  }

  dailyRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-daily",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        daily = json.decode(response.body);
      });
    }
  }

  dAwal() async {
    var response = await http.get(
      Help.deviceApi() + "/device/dashboard-awal",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        awal = json.decode(response.body);
      });
    }
  }

  dAkhir() async {
    var response = await http.get(
      Help.deviceApi() + "/device/dashboard-akhir",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        akhir = json.decode(response.body);
      });
    }
  }

  getPower() async {
    var response = await http.get(
      Help.baseUrlApi() + "/all-power.php",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        powerdata = json.decode(response.body);
      });
    }
  }

  validation() async {
    var powerMaks = powerdata;

    var voltageMaks =
        awal[0]["last_value16"] + (awal[0]["last_value16"] * 10 / 100);
    var voltageMin =
        awal[0]["last_value16"] - (awal[0]["last_value16"] * 10 / 100);

    if (akhir[0]["last_value15"] != 0 && akhir[0]["last_value16"] != 0) {
      if (akhir[0]["last_value15"] > powerMaks &&
              akhir[0]["last_value16"] > voltageMaks ||
          akhir[0]["last_value16"] < voltageMin) {
        setState(() {
          power = false;
          voltage = false;
        });
      } else if (akhir[0]["last_value15"] > powerMaks) {
        setState(() {
          power = false;
          voltage = true;
        });
      } else if (akhir[0]["last_value16"] > voltageMaks ||
          akhir[0]["last_value16"] < voltageMin) {
        setState(() {
          power = true;
          voltage = false;
        });
      } else {
        power = true;
        voltage = true;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    timer =
        new Timer.periodic(new Duration(seconds: 1), (t) => monthlyRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dailyRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAkhir());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAwal());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dateNow);
    timer = new Timer.periodic(new Duration(seconds: 30), (t) => validation());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => getPower());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      drawer: new Drawer(
        child: AppDrawer(),
      ),
      appBar: new AppBar(
        title: new Text("Dashboard"),
        backgroundColor: Color(0xff0049B8),
        actions: <Widget>[
          new IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => _information(context),
                );
              }),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: monthly == null ||
                  awal == null ||
                  akhir == null ||
                  daily == null
              ? Text(
                  "Getting the data...",
                  textAlign: TextAlign.center,
                  style: Style.Default.getData(context),
                )
              : Column(
                  children: <Widget>[
                    new Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Text(
                                "Daily Report",
                                style: GoogleFonts.lato(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                              new Text(
                                dateNow,
                                style: GoogleFonts.lato(
                                  fontSize: 13,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(top: 40),
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Today's energy usage",
                            style: Style.Default.dBoard(context),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  daily[0]["usage"].toString(),
                                  style: Style.Default.welcomeText(context),
                                ),
                                Text(
                                  " kWh",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "Electrical Voltage",
                            style: Style.Default.dBoard(context),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  akhir[0]["last_value16"].toString(),
                                  style: Style.Default.welcomeText(context),
                                ),
                                Text(
                                  " V",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "Electrical Power",
                            style: Style.Default.dBoard(context),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  akhir[0]["last_value15"].toString(),
                                  style: Style.Default.welcomeText(context),
                                ),
                                Text(
                                  " W",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            "This month's energy usage",
                            style: Style.Default.dBoard(context),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  monthly[0]["total"].toString(),
                                  style: Style.Default.welcomeText(context),
                                ),
                                Text(
                                  " kWh",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      color: Colors.red,
                      child: Offstage(
                        offstage: voltage,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
                          child: Text(
                            "WARNING! Electrical voltage is not stable",
                            style: Style.Default.warning(context),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      color: Colors.red,
                      child: Offstage(
                        offstage: power,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
                          child: Text(
                            "WARNING! Electric power exceeds the limits of use",
                            style: Style.Default.warning(context),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Dashboard",
        style: Style.Default.infoTitle(context),
      ),
      content: new Text(
        "Menu dashboard merupakan menu di mana pengguna dapat mengetahui total konsumsi energi listrik pada hari tersebut secara real-time. Terdapat pula total konsumsi energi listrik pada bulan tersebut, serta tegangan dan daya per hari",
        style: Style.Default.info(context),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text(
            "Tap anywhere to dismiss",
            style: TextStyle(
              fontSize: 10,
              color: Colors.blue,
            ),
          ),
        )
      ],
    );
  }
}
