import 'dart:async';
import 'package:energy_monitor/views/homepage.dart';
import 'package:energy_monitor/views/login.dart';
import 'package:energy_monitor/views/onboardingScreen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isLogin = false;

  void navigationPage() {
    _cekBool();
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), navigationPage);
  }

  Future _cekBool() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getBool("isLogin") == null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => OnboardingScreen()),
        (Route<dynamic> route) => false,
      );
    } else if (pref.getBool("isLogin") == false) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Login()),
        (Route<dynamic> route) => false,
      );
    } else {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
        (Route<dynamic> route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(fit: StackFit.expand, children: <Widget>[
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
                flex: 7,
                child: Container(
                    child: Container(
                  child: new Image(image: new AssetImage('images/final.png')),
                  width: 250,
                  height: 250,
                )))
          ])
    ]));
  }
}
