import 'dart:async';
import 'dart:convert';

import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/appDrawer.dart';
import 'package:energy_monitor/views/device/dua.dart';
import 'package:energy_monitor/views/device/empat.dart';
import 'package:energy_monitor/views/device/lima.dart';
import 'package:energy_monitor/views/device/satu.dart';
import 'package:energy_monitor/views/device/tiga.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class Device extends StatefulWidget {
  @override
  _DeviceState createState() => _DeviceState();
}

class _DeviceState extends State<Device> {
  var satu = true;
  var dua = true;
  var tiga = true;
  var empat = true;
  var lima = true;
  var value = 0;
  List datadua;
  List datatiga;
  List dataempat;
  List datalima;
  List dataenam;
  Timer timer;

  satuDetail() async {
    var response2 = await http.get(
      Help.deviceApi() + "/device/get-device-dua",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(
        () {
          datadua = json.decode(response2.body);
        },
      );
    }
  }

  duaDetail() async {
    var response2 = await http.get(
      Help.deviceApi() + "/device/get-device-tiga",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(
        () {
          datatiga = json.decode(response2.body);
        },
      );
    }
  }

  tigaDetail() async {
    var response2 = await http.get(
      Help.deviceApi() + "/device/get-device-empat",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(
        () {
          dataempat = json.decode(response2.body);
        },
      );
    }
  }

  empatDetail() async {
    var response2 = await http.get(
      Help.deviceApi() + "/device/get-device-lima",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(
        () {
          datalima = json.decode(response2.body);
        },
      );
    }
  }

  limaDetail() async {
    var response2 = await http.get(
      Help.deviceApi() + "/device/get-device-enam",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(
        () {
          dataenam = json.decode(response2.body);
        },
      );
    }
  }

  @override
  void initState() {
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => satuDetail());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => duaDetail());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => tigaDetail());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => empatDetail());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => limaDetail());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: new Drawer(
        child: AppDrawer(),
      ),
      appBar: new AppBar(
        title: new Text("Device"),
        backgroundColor: Color(0xff0049B8),
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.info),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => _information(context),
              );
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: datadua == null ||
                datatiga == null ||
                dataempat == null ||
                datalima == null ||
                dataenam == null
            ? Text(
                "Getting the data...",
                textAlign: TextAlign.center,
                style: Style.Default.getData(context),
              )
            : Column(
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        // First Card------------------------------------------------------
                        GestureDetector(
                          child: datadua[0]['device_name'] == "no device"
                              ? Text("")
                              : Card(
                                  child: new ListTile(
                                    title: new Text(
                                      datadua[0]['device_name'].toString(),
                                      style: Style.Default.deviceName(context),
                                      textAlign: TextAlign.center,
                                    ),
                                    subtitle: new Text(
                                      datadua[0]['address'].toString(),
                                      style: TextStyle(fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                          onTap: () {
                            value = 1;
                            setState(
                              () {
                                if (value == 1) {
                                  satu = false;
                                  dua = true;
                                  tiga = true;
                                  empat = true;
                                  lima = true;
                                } else {}
                              },
                            );
                          },
                        ),
                        Container(
                          child: Offstage(
                            offstage: satu,
                            child: Container(
                              child: Satu(),
                            ),
                          ),
                        ),
                        // End of first card------------------------------------------

                        // Second card ---------------------------------------------------
                        GestureDetector(
                          child: datatiga[0]['device_name'] == "no device"
                              ? Text("")
                              : Card(
                                  child: new ListTile(
                                    title: new Text(
                                      datatiga[0]['device_name'].toString(),
                                      style: Style.Default.deviceName(context),
                                      textAlign: TextAlign.center,
                                    ),
                                    subtitle: new Text(
                                      datatiga[0]['address'].toString(),
                                      style: TextStyle(fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                          onTap: () {
                            value = 1;
                            setState(
                              () {
                                if (value == 1) {
                                  satu = true;
                                  dua = false;
                                  tiga = true;
                                  empat = true;
                                  lima = true;
                                } else {}
                              },
                            );
                          },
                        ),
                        Container(
                          child: Offstage(
                            offstage: dua,
                            child: Container(
                              child: Dua(),
                            ),
                          ),
                        ),
                        // End of second card------------------------------------------

                        // Third card ---------------------------------------------------
                        GestureDetector(
                          child: dataempat[0]['device_name'] == "no device"
                              ? Text("")
                              : Card(
                                  child: new ListTile(
                                    title: new Text(
                                      dataempat[0]['device_name'].toString(),
                                      style: Style.Default.deviceName(context),
                                      textAlign: TextAlign.center,
                                    ),
                                    subtitle: new Text(
                                      dataempat[0]['address'].toString(),
                                      style: TextStyle(fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                          onTap: () {
                            value = 1;
                            setState(
                              () {
                                if (value == 1) {
                                  satu = true;
                                  dua = true;
                                  tiga = false;
                                  empat = true;
                                  lima = true;
                                } else {}
                              },
                            );
                          },
                        ),
                        Container(
                          child: Offstage(
                            offstage: tiga,
                            child: Container(
                              child: Tiga(),
                            ),
                          ),
                        ),
                        // End of third card------------------------------------------
                        // Fourth card ---------------------------------------------------
                        GestureDetector(
                          child: datalima[0]['device_name'] == "no device"
                              ? Text("")
                              : Card(
                                  child: new ListTile(
                                    title: new Text(
                                      datalima[0]['device_name'].toString(),
                                      style: Style.Default.deviceName(context),
                                      textAlign: TextAlign.center,
                                    ),
                                    subtitle: new Text(
                                      datalima[0]['address'].toString(),
                                      style: TextStyle(fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                          onTap: () {
                            value = 1;
                            setState(
                              () {
                                if (value == 1) {
                                  satu = true;
                                  dua = true;
                                  tiga = true;
                                  empat = false;
                                  lima = true;
                                } else {}
                              },
                            );
                          },
                        ),
                        Container(
                          child: Offstage(
                            offstage: empat,
                            child: Container(
                              child: Empat(),
                            ),
                          ),
                        ),
                        // End of fourth card------------------------------------------
                        // Fifth card ---------------------------------------------------
                        GestureDetector(
                          child: dataenam[0]['device_name'] == "no device"
                              ? Text("")
                              : Card(
                                  child: new ListTile(
                                    title: new Text(
                                      dataenam[0]['device_name'].toString(),
                                      style: Style.Default.deviceName(context),
                                      textAlign: TextAlign.center,
                                    ),
                                    subtitle: new Text(
                                      dataenam[0]['address'].toString(),
                                      style: TextStyle(fontSize: 15),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                          onTap: () {
                            value = 1;
                            setState(
                              () {
                                if (value == 1) {
                                  satu = true;
                                  dua = true;
                                  tiga = true;
                                  empat = true;
                                  lima = false;
                                } else {}
                              },
                            );
                          },
                        ),
                        Container(
                          child: Offstage(
                            offstage: lima,
                            child: Container(
                              child: Lima(),
                            ),
                          ),
                        ),
                        // End of fifth card------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Device",
        style: Style.Default.infoTitle(context),
      ),
      content: new Text(
        "Menu Device adalah Menu di mana pengguna dapat melihat total perangkat yang di monitor menggunakan smart energy monitoring. Data yang ditampilkan adalah nama serta alamat device. Pengguna dapat menekan device yang tertera yang kemudian nantinya akan menampilkan total penggunaan energi listrik serta tegangan listrik dari device itu sendiri.",
        style: Style.Default.info(context),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text("Tap anywhere to dismiss",
              style: TextStyle(
                fontSize: 10,
                color: Colors.blue,
              )),
        )
      ],
    );
  }
}
