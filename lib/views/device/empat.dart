import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:energy_monitor/components/help.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:energy_monitor/css/style.dart' as Style;

class Empat extends StatefulWidget {
  @override
  _EmpatState createState() => _EmpatState();
}

class _EmpatState extends State<Empat> {
  var voltage = true;
  var x = 0;

  List jsondata;
  List awal;
  List akhir;
  Timer timer;

  makeRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/emonito-device-lima-usage",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata = json.decode(response.body);
      });
    }
  }

  dAwal() async {
    var response = await http.get(
      Help.deviceApi() + "/device/device-lima-awal",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        awal = json.decode(response.body);
      });
    }
  }

  dAkhir() async {
    var response = await http.get(
      Help.deviceApi() + "/device/device-lima-akhir",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        akhir = json.decode(response.body);
      });
    }
  }

  validation() async {
    var voltageMaks = awal[0]["last_value16"]+(awal[0]["last_value16"]*10/100);
    var voltageMin = awal[0]["last_value16"]-(awal[0]["last_value16"]*10/100);

    if (akhir[0]["last_value16"] != 0) {
      if (akhir[0]["last_value16"] > voltageMaks ||
          akhir[0]["last_value16"] < voltageMin) {
        setState(() {
          voltage = false;
        });
      } else {
        voltage = true;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => makeRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAwal());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAkhir());
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => validation());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Container(
        child: jsondata == null || awal == null || akhir == null
            ? Text(
                "Getting the data ...",
                textAlign: TextAlign.center,
                style: Style.Default.getData(context),
              )
            : Column(
                children: <Widget>[
                  Container(
                    // color: Colors.red,
                    margin: EdgeInsets.all(3),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Energy Usage",
                                    style: Style.Default.device(context),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    jsondata[0]["usage"].toString() + " kWh",
                                    style: Style.Default.loginText(context),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Electrical Voltage",
                                    style: Style.Default.device(context),
                                    textAlign: TextAlign.center,
                                  ),
                                  Text(
                                    akhir[0]["last_value16"].toString() + " V",
                                    style: Style.Default.loginText(context),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          color: Colors.red,
                          child: Offstage(
                            offstage: voltage,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 10, 5, 5),
                              child: Text(
                                "WARNING! Electrical voltage is not stable",
                                style: Style.Default.warning(context),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
