import 'dart:async';
import 'package:energy_monitor/views/comparison/weekly/eCharts.dart';
import 'package:energy_monitor/views/comparison/weekly/eTable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class ComparisonWeekly extends StatefulWidget {
  @override
  _ComparisonWeekly createState() => _ComparisonWeekly();
}

class _ComparisonWeekly extends State<ComparisonWeekly> {
  var now = new DateTime.now();
  DateTime sixDays = DateTime.now().subtract(Duration(days: 6));
  DateTime tWeeks = DateTime.now().subtract(Duration(days: 27));
  DateTime tWeeks2 = DateTime.now().subtract(Duration(days: 21));
  var showEnergyCharts = true;
  var showEnergyTable = true;
  var value = 0;
  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => now);
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => tWeeks);
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => tWeeks2);
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => sixDays);
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: Container(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                        style: new TextStyle(fontSize: 15, color: Colors.black),
                        children: <TextSpan>[
                          new TextSpan(
                              text: "This features shows the comparison from "),
                          new TextSpan(
                              text: "three weeks ago (" +
                                  new DateFormat("d MMM").format(tWeeks) +
                                  " - " +
                                  new DateFormat("d MMM yyyy").format(tWeeks2) +
                                  " )",
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: " until "),
                          new TextSpan(
                              text: "this week (" +
                                  new DateFormat("d MMM").format(sixDays) +
                                  " - " +
                                  new DateFormat("d MMM yyyy").format(now) +
                                  " )",
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                        ]),
                  ),
                ),
              ),

              Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    //Start of Box Electrical Energy Usage
                    GestureDetector(
                      onTap: () {
                        value = 1;

                        setState(() {
                          if (value == 1) {
                            showEnergyTable = true;
                            showEnergyCharts = false;
                          } else {}
                        });
                      },
                      child: Container(
                        child: Text(
                          "Show in Charts",
                          style: Style.Default.comparison(context),
                          textAlign: TextAlign.center,
                        ),
                        color: Color(0xff9EC4FA),
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, left: 10, right: 10),
                        width: MediaQuery.of(context).size.width / 2.5,
                      ),
                    ),
                    //End of Box Electrical Energy Usage
                    //Start of Box Electrical Voltage Usage
                    GestureDetector(
                      onTap: () {
                        value = 2;

                        setState(() {
                          if (value == 2) {
                            showEnergyTable = false;
                            showEnergyCharts = true;
                          } else {}
                        });
                      },
                      child: Container(
                        child: Text(
                          "Show in Table",
                          style: Style.Default.comparison(context),
                          textAlign: TextAlign.center,
                        ),
                        color: Color(0xff9EC4FA),
                        padding: EdgeInsets.only(
                            top: 10, bottom: 10, left: 10, right: 10),
                        width: MediaQuery.of(context).size.width / 2.5,
                      ),
                    ),
                    //Start of Box Electrical Voltage Usage
                  ],
                ),
              ),
              //OFFSTAGE ENERGY USAGE - Show Charts
              Container(
                child: Offstage(
                  offstage: showEnergyCharts,
                  child: Container(
                    child: ECharts(),
                  ),
                ),
              ),
              //OFFSTAGE ENERGY USAGE - Show Charts
              //OFFSTAGE ENERGY USAGE - Show Table
              Container(
                child: Offstage(
                  offstage: showEnergyTable,
                  child: ETable(),
                ),
              ),
              //OFFSTAGE ENERGY USAGE - Show Table
            ],
          ),
        ),
      ),
    );
  }
}
