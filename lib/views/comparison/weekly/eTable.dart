import 'dart:async';
import 'dart:convert';
import 'package:energy_monitor/components/help.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class ETable extends StatefulWidget {
  @override
  _ETableState createState() => _ETableState();
}

class _ETableState extends State<ETable> {
  List data;
  Timer timer;

  Future<String> getDeviceLog() async {
    var url = Help.deviceApi() + "/device/comparison-weekly";
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    if (this.mounted) {
      setState(() {
        data = json.decode(response.body);
      });
    }

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => getDeviceLog());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Center(
        child: data == null
            ? Text(
                "Getting the data...",
                textAlign: TextAlign.center,
                style: Style.Default.getData(context),
              )
            : Column(
                children: <Widget>[
                  Text(
                    "Electrical Energy Usage Comparison",
                    style: Style.Default.comparison2(context),
                  ),
                  Center(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 20 , 0),
                      padding: EdgeInsets.all(10),
                      color: Color(0xff151965),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Date",
                            style: Style.Default.comparisonTable(context),
                          ),
                          Text(
                            "Total Energy Usage",
                            style: Style.Default.comparisonTable(context),
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    color: Color(0xff9EC4FA),
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    height: 200,
                    child: _buildListView(),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          return _buildRow(data[index]);
        });
  }

  Widget _buildRow(item) => Container(
        child: Column(children: <Widget>[
          Container(
            padding: EdgeInsets.all(15),
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(item['week'].toString()),
                  Text(item['total'].toString() + " Kwh"),
                ],
              )
            ]),
          ),
        ]),
      );
}