import 'package:energy_monitor/views/appDrawer.dart';
import 'package:energy_monitor/views/comparison/daily/daily.dart';
import 'package:energy_monitor/views/comparison/monthly/monthly.dart';
import 'package:energy_monitor/views/comparison/weekly/weekly.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class Comp extends StatefulWidget {
  @override
  _CompState createState() => _CompState();
}

class _CompState extends State<Comp> with SingleTickerProviderStateMixin {
  List<Widget> pages = [
    ComparisonDaily(),
    ComparisonWeekly(),
    ComparisonMonthly(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        drawer: new Drawer(
          child: AppDrawer(),
        ),
        appBar: new AppBar(
          title: new Text("Comparison"),
          backgroundColor: Color(0xff0049B8),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => _information(context),
                  );
                }),
          ],
          bottom: TabBar(
              unselectedLabelColor: Colors.blue,
              labelColor: Colors.yellow,
              indicatorColor: Colors.yellow,
              indicatorWeight: 3.0,
              tabs: <Widget>[
                Tab(text: "Daily"),
                Tab(text: "Weekly"),
                Tab(text: "Monthly")
              ]),
        ),
        body: TabBarView(children: pages),
      ),
    );
  }

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Comparison",
        style: Style.Default.infoTitle(context),
      ),
      content: new Text(
        "Menu Comparison adalah Menu di mana pengguna dapat melihat perbandingan penggunaan energi listrik per hari, minggu, serta bulan. Perbandingan penggunaan energi listrik ditampilkan dengan menggunakan grafik dan tabel",
        style: TextStyle(
          fontSize: 13,
          color: Colors.black,
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text("Tap anywhere to dismiss",
              style: TextStyle(
                fontSize: 10,
                color: Colors.blue,
              )),
        )
      ],
    );
  }
}
