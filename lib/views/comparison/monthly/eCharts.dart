import 'dart:async';

import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/models/charts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:convert';
import 'package:energy_monitor/css/style.dart' as Style;

class ECharts extends StatefulWidget {
  @override
  _EChartsState createState() => _EChartsState();
}

class _EChartsState extends State<ECharts> {
  var now = new DateTime.now();
  DateTime lastMonth = DateTime.now().subtract(Duration(days: 30));
  List jsondata;
  Timer timer;

  makeRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-monthly-chart",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata = json.decode(response.body);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => makeRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => now);
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => lastMonth);
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  charts.Series<CD, String> createSeries(String id, int i) {
    return charts.Series<CD, String>(
      id: "haha",
      domainFn: (CD cons, _) => cons.hour,
      measureFn: (CD cons, _) => cons.total,
      colorFn: (_, __) => charts.MaterialPalette.indigo.shadeDefault,
      data: [CD(jsondata[i]["month"].toString(), jsondata[i]["total"])],
    );
  }

  Widget createChart() {
    List<charts.Series<CD, String>> seriesList = [];

    for (int i = 0; i < jsondata.length; i++) {
      String id = '${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(seriesList,
        barGroupingType: charts.BarGroupingType.stacked);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: jsondata == null
          ? Text(
              "Getting the data...",
              textAlign: TextAlign.center,
              style: Style.Default.getData(context),
            )
          : Column(
              children: <Widget>[
                new Text(
                  "Electrical Energy Usage Comparison",
                  style: Style.Default.comparison2(context),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: new Text(
                    "The total use of electrical energy is displayed using units of watt / hour. The data in kilowatt / hour (kWh) units is shown in the table. (click the 'show in table' button above)",
                    style: Style.Default.chartsInfo(context),
                  ),
                ),
                new Container(
                  height: 300,
                  child: createChart(),
                ),
              ],
            ),
    );
  }
}
