import 'dart:async';
import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/appDrawer.dart';
import 'package:energy_monitor/views/dboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List jsondata;
  List jsondata5;
  List jsondata6;
  Timer timer;
  List awal;
  List akhir;
  var pricedata;
  var powerdata;
  List reportdata;
  int x = -1;

  String message = '';
  bool isLoading = false;
  DateTime currentBackPressTime;

  //save account usename dan name
  String accountUsername = "";
  String accountName = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
        accountName = pref.getString("name");
      });
    } else {
      setState(() {
        accountUsername = "";
        accountName = "";
      });
    }
  }

  String _timeString;
  String _usageString;
  String _reportString;

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    final String formatUsageTime = _formatUsageTime(now);
    final String formatReportTime = _formatReportTime(now);
    setState(() {
      _timeString = formattedDateTime;
      _usageString = formatUsageTime;
      _reportString = formatReportTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat("MM/dd/yyyy hh:mm a").format(dateTime);
  }

  String _formatUsageTime(DateTime dateTime) {
    return DateFormat("MM/dd/yyyy 09:00 a").format(dateTime);
  }

  String _formatReportTime(DateTime dateTime) {
    return DateFormat("MM/10/yyyy 08:00 a").format(dateTime);
  }

  validate() async {
    if (_timeString == _usageString) {
      showNotification();
    } else {}
  }

  validate2() async {
    if (_timeString == _reportString) {
      if (x != -1) {
        showNotification2();
      }
    } else {}
  }

  getPower() async {
    var response = await http.get(
      Help.baseUrlApi() + "/all-power.php",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        powerdata = json.decode(response.body);
      });
    }
  }

  validationVoltage() async {
    var voltageMaks = awal[0]["last_value16"]+(awal[0]["last_value16"]*10/100);
    var voltageMin = awal[0]["last_value16"]-(awal[0]["last_value16"]*10/100);
    var powerMaks = powerdata;

    if (akhir[0]["last_value16"] != 0) {
      if (akhir[0]["last_value16"] > voltageMaks ||
          akhir[0]["last_value16"] < voltageMin ||
          akhir[0]["last_value15"] > powerMaks) {
        showNotification3();
      } else {}
    } else {}
  }

  count() async {
    if (powerdata <= 2200) {
      x = ((reportdata[0]["hasil"] * pricedata) +
              (reportdata[0]["hasil"] * 6 / 100) +
              3000 +
              6000)
          .round();
    } else {
      x = ((reportdata[0]["hasil"] * pricedata) +
              (reportdata[0]["hasil"] * 10 / 100) +
              3000 +
              6000)
          .round();
    }

    return x;
  }

  @override
  void initState() {
    super.initState();
    _userData();

    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    timer = new Timer.periodic(new Duration(seconds: 3), (t) => getDevice());
    timer = new Timer.periodic(new Duration(seconds: 3), (t) => makeRequest());
    timer = new Timer.periodic(new Duration(minutes: 1), (t) => monthlyRequest());
    timer = new Timer.periodic(new Duration(minutes: 1), (t) => weeklyRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAwal());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => count());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dAkhir());
    Timer.periodic(Duration(seconds: 30), (Timer t) => validate());
    Timer.periodic(Duration(seconds: 30), (Timer t) => validate2());
    Timer.periodic(Duration(minutes: 1), (Timer t) => validationVoltage());
    Timer.periodic(Duration(minutes: 3), (Timer t) => validationMonthly());
    Timer.periodic(Duration(seconds: 5), (Timer t) => validationWeekly());
    Timer.periodic(Duration(minutes: 2), (Timer t) => getPower());

    //Code untuk Notifikasi
    initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');

    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    //Code untuk Notifikasi
  }

  validationMonthly() async {
    if (jsondata5[0]["total"] > jsondata5[1]["total"]) {
      showNotification5();
    } else {}
  }

  validationWeekly() async {
    if (jsondata6[0]["total"] > jsondata6[1]["total"]) {
      showNotification6();
    } else {}
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  //Code untuk Notifikasi

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint("Notification payload: $payload");
    }
    await Navigator.push(
        context, new MaterialPageRoute(builder: (context) => new Dboard()));
  }

  getDevice() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-daily",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata = json.decode(response.body);
      });
    }
  }

  dAwal() async {
    var response = await http.get(
      Help.deviceApi() + "/device/dashboard-awal",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        awal = json.decode(response.body);
      });
    }
  }

  makeRequest() async {
    var response = await http.get(
      Help.baseUrlApi() + "/all-price.php",
      headers: {'Accept': 'application/json'},
    );

    var response2 = await http.get(
      Help.deviceApi() + "/device/report-usage",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        pricedata = json.decode(response.body);
        reportdata = json.decode(response2.body);
      });
    }
  }

  dAkhir() async {
    var response = await http.get(
      Help.deviceApi() + "/device/dashboard-akhir",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        akhir = json.decode(response.body);
      });
    }
  }

  monthlyRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-monthly",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata5 = json.decode(response.body);
      });
    }
  }

  weeklyRequest() async {
    var response = await http.get(
      Help.deviceApi() + "/device/comparison-weekly",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata6 = json.decode(response.body);
      });
    }
  }

  

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

  Future<void> showNotification() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'channel_ID', 'channel name', 'channel description',
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'test ticker');
    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        "Total use of electrical energy today",
        jsondata[0]["usage"].toString() + " kWh",
        platformChannelSpesifics,
        payload: "ini payload");
  }

  Future<void> showNotification2() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'channel_ID', 'channel name', 'channel description',
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'test ticker');
    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        "Electricity Bill",
        NumberFormat.simpleCurrency(locale: 'in', decimalDigits: 0)
            .format(x),
        platformChannelSpesifics,
        payload: "ini payload");
  }

  Future<void> showNotification3() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID',
      'channel name',
      'channel description',
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        "WARNING!",
        "Something went wrong. Please check dashboard menu",
        platformChannelSpesifics,
        payload: "ini payload");
  }

  Future<void> showNotification5() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID',
      'channel name',
      'channel description',
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        "WARNING!",
        "This month's electricity usage is higher than last month's",
        platformChannelSpesifics,
        payload: "ini payload");
  }

  Future<void> showNotification6() async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID',
      'channel name',
      'channel description',
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSChannelSpesifics = IOSNotificationDetails();
    var platformChannelSpesifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSChannelSpesifics);

    await flutterLocalNotificationsPlugin.show(
        0,
        "WARNING!",
        "This week's electricity usage is higher than last week's",
        platformChannelSpesifics,
        payload: "ini payload");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: new Drawer(
        child: AppDrawer(),
      ),
      body: Dboard(),
    );
  }
}
