import 'package:energy_monitor/views/homepage.dart';
import 'package:energy_monitor/views/profile/success.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/components/help.dart';

class DeactiveAcc extends StatefulWidget {
  @override
  _DeactiveAccState createState() => _DeactiveAccState();
}

class _DeactiveAccState extends State<DeactiveAcc> {
  String accountUsername = "";
  String accountName = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
        accountName = pref.getString("name");
      });
    } else {
      setState(() {
        accountUsername = "";
        accountName = "";
      });
    }
  }

  void submit() {
    var url = Help.baseUrlApi() + "/user-deactive.php";

    http.post(
      url,
      body: {
        "username": accountUsername,
      },
    );
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => Success(),
      ),
      (Route<dynamic> route) => false,
    );

    sendEmail();
  }

  void sendEmail() {
    var url = Help.baseUrlApi() + "/mail-deactive.php";

    http.post(
      url,
      body: {
        "username": accountUsername,
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _userData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.red,
        body: Stack(fit: StackFit.expand, children: <Widget>[
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text("WARNING!",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.yellow,
                        ))),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    "Once you deactive your account, you have to contact our customer service to activated your account",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10),
                        ),
                        child: Text("Return to Homepage",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            )),
                        onPressed: () {
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomePage(),
                            ),
                            (Route<dynamic> route) => false,
                          );
                        }),
                    Container(
                      width: 10,
                    ),
                    RaisedButton(
                        color: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10),
                        ),
                        child: Text("Deactive Account",
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            )),
                        onPressed: () {
                          submit();
                        }),
                  ],
                ),
              ])
        ]));
  }
}
