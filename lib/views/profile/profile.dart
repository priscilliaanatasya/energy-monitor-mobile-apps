import 'package:energy_monitor/views/profile/editProfile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:energy_monitor/css/style.dart' as Style;

import '../appDrawer.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

var dataUser = new List();

class _ProfileState extends State<Profile> {
  var imageUser = Image.asset('images/profile.png');

  //save account usename dan name
  String accountUsername = "";
  String accountName = "";
  String accountEmail = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
        accountName = pref.getString("name");
        accountEmail = pref.getString("email");
      });
    } else {
      setState(() {
        accountUsername = "";
        accountName = "";
        accountEmail = "";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _userData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: new Drawer(
          child: AppDrawer(),
        ),
        appBar: new AppBar(
          title: new Text("Profile"),
          backgroundColor: Color(0xff0049B8),
        ),
        body: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.end, children: <
                    Widget>[
          Container(
            margin: EdgeInsets.all(10),
            child: InkWell(
              child: Text(
                "Edit Profile",
                style: TextStyle(color: Colors.blue, fontSize: 15),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EditProfile()),
                );
              },
            ),
          ),
          Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 30, bottom: 20),
                  child: new Image(image: new AssetImage('images/final.png')),
                  width: 200,
                ),
                Container(
                    padding: EdgeInsets.only(bottom: 40),
                    child: Column(children: <Widget>[
                      Text(
                        "Hallo, " + accountUsername + " !",
                        style: Style.Default.loginText(context),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 40, right: 40, top: 40),
                          child: Column(children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Full name "),
                                Text(accountName),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Username "),
                                Text(accountUsername),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                            ),
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Email "),
                                  Text(accountEmail),
                                ])
                          ]))
                    ]))
              ]))
        ])));
  }
}
