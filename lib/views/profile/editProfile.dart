import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/login.dart';
import 'package:energy_monitor/views/profile/deactive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController _editPasswordController = TextEditingController();
  TextEditingController _editEmailController = TextEditingController();

  var emailValue = 0;
  var passValue = 0;

  String newEmail = "";
  String newPass = "";
  final _key = new GlobalKey<FormState>();

  String accountUsername = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
      });
    } else {
      setState(() {
        accountUsername = "";
      });
    }
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      validateEdit();
    } else {}
  }

  validateEdit() {
    if (newEmail != null && newPass != null) {
      submitEmail();
      submitPassword();

      //alert dialog berhasil
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.blue,
            title: Text(
              "Profile Updated!",
              style: Style.Default.editProfile(context),
            ),
            content: Text("Please Login Again"),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  _logout();
                },
                child: const Text(
                  'OK',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          );
        },
      );
    } else if (newEmail != null && newPass == null) {
      submitEmail();

      //alert dialog berhasil
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.blue,
            title: Text(
              "Profile Updated!",
              style: Style.Default.editProfile(context),
            ),
            content: Text("Please Login Again"),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  _logout();
                },
                child: const Text(
                  'OK',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          );
        },
      );
    } else if (newEmail == null && newPass != null) {
      submitPassword();

      //alert dialog berhasil
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.blue,
            title: Text(
              "Profile Updated!",
              style: Style.Default.editProfile(context),
            ),
            content: Text("Please Login Again"),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  _logout();
                },
                child: const Text(
                  'OK',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          );
        },
      );
    } else if (newEmail == null && newPass == null) {
      //alert dialog gagal
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Color(0xff84142d),
            title: Text(
              "No Changes Detected!",
              style: Style.Default.editProfile(context),
            ),
            content: Text(
              "Please add some changes on email or password",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(
                  'OK',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          );
        },
      );
    }
  }

  void submitEmail() {
    var url = Help.baseUrlApi() + "/update_email.php";

    http.post(
      url,
      body: {"email": _editEmailController.text, "username": accountUsername},
    );
  }

  void submitPassword() {
    var url = Help.baseUrlApi() + "/update_password2.php";

    http.post(
      url,
      body: {
        "password_hash":_editPasswordController.text,
        "username": accountUsername,
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _userData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  //logout
  Future _logout() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("isLogin", false);
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => new Login()),
      (Route<dynamic> route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _key,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: false,
            appBar: new AppBar(
              title: new Text("Edit Profile"),
              backgroundColor: Color(0xff0049B8),
            ),
            body: Container(
                margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                              "You are logged in as ",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15,
                              ),
                            ),
                            new Text(
                              accountUsername + ".",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            new InkWell(
                                child: Text(
                                  " Not you? Log out.",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                  ),
                                ),
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                            backgroundColor: Color(0xff84142d),
                                            title: Text(
                                              "Are you sure want to logout?",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            actions: <Widget>[
                                              Row(children: <Widget>[
                                                FlatButton(
                                                  onPressed: () {
                                                    _logout();
                                                  },
                                                  child: const Text(
                                                    'Yes',
                                                    style: TextStyle(
                                                        color: Colors.yellow),
                                                  ),
                                                ),
                                                FlatButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('No',
                                                        style: TextStyle(
                                                          color: Colors.grey,
                                                        )))
                                              ])
                                            ]);
                                      });
                                })
                          ]),
                      Column(children: <Widget>[
                        new Padding(
                            padding: EdgeInsets.only(top: 50.0),
                            child: new Text("New Email",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                )))
                      ]),
                      new TextFormField(
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            filled: false,
                            hintText: "ex: emonito123@example.com",
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          controller: _editEmailController,
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              newEmail = null;
                              return null;
                            } else {
                              newEmail = value;
                              return null;
                            }
                          }),
                      new Padding(
                          padding: EdgeInsets.only(top: 30.0),
                          child: new Text("New Password",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ))),
                      new TextFormField(
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            filled: false,
                            hintText: "ex: emonito2020",
                            hintStyle: TextStyle(color: Colors.grey),
                          ),
                          controller: _editPasswordController,
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              newPass = null;
                              return null;
                            } else {
                              newPass = value;
                              return null;
                            }
                          }),
                      new Container(
                          padding: EdgeInsets.only(top: 20),
                          width: double.maxFinite,
                          child: new RaisedButton(
                              color: Color(0xff0049B8),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(20),
                              ),
                              child: Text(
                                "Save Changes",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                              ),
                              onPressed: () {
                                check();
                              })),
                      Center(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: InkWell(
                            child: Text(
                              "Deactive Account",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DeactiveAcc()),
                              );
                            },
                          ),
                        ),
                      ),
                    ]))));
  }
}
