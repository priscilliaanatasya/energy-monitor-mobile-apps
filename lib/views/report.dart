import 'dart:async';
import 'dart:convert';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/appDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class Report extends StatefulWidget {
  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {
  var showTotalEnergy = true;
  var showInfo = false;

  var value = 0;
  var dateNow = new DateFormat('MMM, yyyy').format(new DateTime.now());

  var jsondata;
  List jsondata2;
  var jsondata3;
  List data;
  Timer timer;
  int x = -1;

  makeRequest() async {
    var response = await http.get(
      Help.baseUrlApi() + "/all-price.php",
      headers: {'Accept': 'application/json'},
    );

    var response2 = await http.get(
      Help.deviceApi() + "/device/report-usage",
      headers: {'Accept': 'application/json'},
    );

    var response3 = await http.get(
      Help.baseUrlApi() + "/all-power.php",
      headers: {'Accept': 'application/json'},
    );

    if (this.mounted) {
      setState(() {
        jsondata = json.decode(response.body);
        jsondata2 = json.decode(response2.body);
        jsondata3 = json.decode(response3.body);
      });
    }
  }

  count() async {
    // if (jsondata3 <= 2200) {
    //   x = ((jsondata2[0]["hasil"] * jsondata) +
    //           (jsondata2[0]["hasil"] * 6 / 100) +
    //           3000 +
    //           6000)
    //       .round();

    //   // x = (jsondata2[0]["hasil"]*jsondata).round();

    // } else {
    //   x = ((jsondata2[0]["hasil"] * jsondata) +
    //           (jsondata2[0]["hasil"] * 10 / 100) +
    //           3000 +
    //           6000)
    //       .round();
    // }
    x = 100000;

    return x;
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => makeRequest());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => count());
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => dateNow);
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        drawer: new Drawer(
          child: AppDrawer(),
        ),
        appBar: new AppBar(
          title: new Text("Report"),
          backgroundColor: Color(0xff0049B8),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => _information(context),
                  );
                }),
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.all(20),
                child: jsondata == null || jsondata2 == null || x <= -1
                    ? Text(
                        "Getting the data...",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.grey,
                        ),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                            new Container(
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new Text(
                                        "Monthly Report",
                                        style: GoogleFonts.lato(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 10),
                                    child: Text(
                                      "The figure below shows the estimated total payment for electricity usage last month",
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: new Image(
                                              image: new AssetImage(
                                                  'images/bill.png')),
                                          width: 30,
                                          height: 30,
                                        ),
                                        Text(
                                          NumberFormat.simpleCurrency(
                                                  locale: 'in',
                                                  decimalDigits: 0)
                                              .format(x),
                                          style: TextStyle(
                                            color: Colors.indigo,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(5),
                                    color: Color(0xffe1f2fb),
                                    child: Container(
                                      margin: EdgeInsets.all(15),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "Tips:",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                          Text(
                                              "To know the total electricity cost that you need to pay, you have to know about admin costs, stamp duty, value added tax, street light tax, total electricity consumption last month, and the electricity pricing."),
                                          Text(
                                              "\nYour total electricity usage last month is:"),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 5, bottom: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  jsondata2[0]["hasil"]
                                                      .toString(),
                                                  style: TextStyle(
                                                    fontSize: 25,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                Text(
                                                  " kWh",
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: GestureDetector(
                                              child: Text(
                                                "Click here to know the electricity pricing",
                                                style: TextStyle(
                                                    color: Colors.blue,
                                                    fontSize: 10,
                                                    fontWeight: FontWeight.bold,
                                                    decoration: TextDecoration
                                                        .underline),
                                              ),
                                              onTap: () => launch(
                                                  "https://web.pln.co.id/pelanggan/tarif-tenaga-listrik/tariff-adjustment"),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ]))
                          ]))));
  }

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Report",
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Color(0xff151965),
        ),
      ),
      content: new Text(
        "Menu Report adalah menu laporan bulanan di mana pengguna dapat mengetahui perkiraan biaya listrik yang akan dibayarkan pada bulan tersebut. Selain itu, pengguna juga dapat mengetahui total penggunaan energi listrik pada bulan tersebut",
        style: TextStyle(
          fontSize: 13,
          color: Colors.black,
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text("Tap anywhere to dismiss",
              style: TextStyle(
                fontSize: 10,
                color: Colors.blue,
              )),
        )
      ],
    );
  }
}
