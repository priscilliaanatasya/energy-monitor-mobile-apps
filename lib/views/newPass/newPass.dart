import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/login.dart';
import 'package:energy_monitor/views/newPass/success.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class NewPassword extends StatefulWidget {
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  TextEditingController _passKeyController = TextEditingController();
  final _passKey = GlobalKey<FormState>();

  String code = "";

  Future _codeData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("password_reset_token") != null) {
      setState(() {
        code = pref.getString("password_reset_token");
      });
    } else {
      setState(() {
        code = "";
      });
    }
  }

  check() {
    final form = _passKey.currentState;
    if (form.validate()) {
      form.save();

      submit();
    } else {}
  }

  void submit() {
    var url = Help.baseUrlApi() + "/update_password.php";

    http.post(
      url,
      body: {
        "password_hash": _passKeyController.text,
        "password_reset_token": code,
      },
    );

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => Success(),
      ),
      (Route<dynamic> route) => false,
    );
  }

  @override
  void initState() {
    _codeData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        body: Form(
            key: _passKey,
            child: Container(
                margin: EdgeInsets.all(30),
                color: Colors.white,
                child: Stack(children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 10.0,
                                    ),
                                    child: Text("Enter Your New Password",
                                        style: TextStyle(
                                          fontSize: 18,
                                        )))),
                            TextFormField(
                                controller: _passKeyController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    filled: false,
                                    hintText: "ex: emonitoUser1234",
                                    hintStyle: TextStyle(
                                      color: Colors.grey,
                                    )),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Please write your new password";
                                  } else {
                                    return null;
                                  }
                                }),
                            Container(
                              padding: EdgeInsets.all(20),
                              child: Center(
                                child: RaisedButton(
                                    padding: EdgeInsets.all(10),
                                    color: Colors.blue,
                                    child: Text(
                                      "Submit",
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              backgroundColor: Colors.white,
                                              title: Center(
                                                  child: Text(
                                                "Update Your Password?",
                                                style: Style.Default.deviceName(
                                                    context),
                                              )),
                                              actions: <Widget>[
                                                RaisedButton(
                                                    color: Colors.blue,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          new BorderRadius
                                                              .circular(10),
                                                    ),
                                                    child: Text("Yes",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 15,
                                                          color: Colors.white,
                                                        )),
                                                    onPressed: () {
                                                      check();
                                                    }),
                                                RaisedButton(
                                                    color: Colors.red,
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                .circular(10)),
                                                    child: Text("No",
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 15,
                                                          color: Colors.white,
                                                        )),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    })
                                              ],
                                            );
                                          });
                                    }),
                              ),
                            ),
                          ])),
                  Positioned(
                      top: 5.0,
                      right: 5.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Login(),
                              ),
                              (Route<dynamic> route) => false,
                            );
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: CircleAvatar(
                                  radius: 20.0,
                                  backgroundColor: Colors.red,
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  )))))
                ]))));
  }
}
