import 'dart:convert';

import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/newPass/newPass.dart';
import 'package:energy_monitor/views/login.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class ResetCode extends StatefulWidget {
  @override
  _ResetCodeState createState() => _ResetCodeState();
}

var dataCode = new List();

class _ResetCodeState extends State<ResetCode> {
  TextEditingController _codeKeyController = TextEditingController();
  final _codeKey = GlobalKey<FormState>();

  String code = "";
  bool isLoading = false;

  Future<List> _code() async {
    var url = Help.baseUrlApi() + "/all-user.php";
    var response = await http.get(
      Uri.encodeFull(url),
      headers: {"Accept": "application/json"},
    );

    var data = jsonDecode(response.body);

    var codeList = new List();

    var x = 0;
    while (x < data.length) {
      codeList.add(data[x]['password_reset_token']);
      x++;
    }

    var result = new List(1);
    result[0] = codeList;

    return result;
  }

  check() async {
    final form = _codeKey.currentState;
    if (form.validate()) {
      var x = 0;
      while (x < dataCode[0].length) {
        if (dataCode[0][x] == (_codeKeyController.text)) {
          code = dataCode[0][x];
          FocusScope.of(context).requestFocus(new FocusNode());

          setState(() {
            isLoading = true;
          });

          Future.delayed(
            Duration(seconds: 1),
            () async {
              SharedPreferences pref = await SharedPreferences.getInstance();
              pref.setString("password_reset_token", code);

              setState(() {
                isLoading = false;
              });

              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => NewPassword(),
                ),
                (Route<dynamic> route) => false,
              );
              x--;
            },
          );

          break;
        }
        x++;
        if (x != -1) {
          setState(() {
            isLoading = true;
          });

          Future.delayed(Duration(seconds: 2), () async {
            setState(() {
              showInfoFlushbar(context);
              isLoading = false;
            });
          });
        }
      }
    } else {}
  }

  @override
  void initState() {
    _code().then(
      (value) {
        dataCode = value;
      },
    );
    isLoading = false;
    super.initState();
  }

  void showInfoFlushbar(BuildContext context) {
    Flushbar(
      title: "Wrong Code!",
      message: "The code that you entered doesn't match to any user",
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Colors.red,
      ),
      leftBarIndicatorColor: Colors.red,
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        body: Form(
            key: _codeKey,
            child: Container(
                margin: EdgeInsets.all(30),
                color: Colors.white,
                child: Stack(children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  bottom: 10.0,
                                ),
                                child: Text(
                                  "Enter Your Password Reset Code",
                                  style: Style.Default.addFeedback(context),
                                ),
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                  bottom: 20.0,
                                ),
                                child: Text(
                                    "A code has been sent to your email. Check your email and paste the code below",
                                    style: TextStyle(
                                      fontSize: 13,
                                    ))),
                            TextFormField(
                                controller: _codeKeyController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  filled: false,
                                  hintText: "Password Reset Code",
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Please add a code";
                                  } else {
                                    return null;
                                  }
                                }),
                            Container(
                                padding: EdgeInsets.all(20),
                                child: Center(
                                    child: RaisedButton(
                                        padding: EdgeInsets.all(10),
                                        color: Colors.blue,
                                        child: Text(
                                          "Submit",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        onPressed: () async {
                                          check();
                                        })))
                          ])),
                  Positioned(
                      top: 5.0,
                      right: 5.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Login(),
                              ),
                              (Route<dynamic> route) => false,
                            );
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: CircleAvatar(
                                  radius: 20.0,
                                  backgroundColor: Colors.red,
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  )))))
                ]))));
  }
}
