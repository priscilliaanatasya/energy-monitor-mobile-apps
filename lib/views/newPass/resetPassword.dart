import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/login.dart';
import 'package:energy_monitor/views/newPass/resetCode.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController _unameKeyController = TextEditingController();
  final _unameKey = GlobalKey<FormState>();

  check() {
    final form = _unameKey.currentState;
    if (form.validate()) {
      submit();
      form.save();
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => ResetCode(),
        ),
        (Route<dynamic> route) => false,
      );
    } else {}
  }

  void submit() {
    var url = Help.baseUrlApi() + "/mail.php";

    http.post(
      url,
      body: {
        "username": _unameKeyController.text,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        body: Form(
            key: _unameKey,
            child: Container(
                margin: EdgeInsets.all(30),
                color: Colors.white,
                child: Stack(children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Center(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 10.0,
                                    ),
                                    child: Text("Enter Your Username Below",
                                        style: TextStyle(
                                          fontSize: 18,
                                        )))),
                            TextFormField(
                              controller: _unameKeyController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                filled: false,
                                hintText: "ex: emonitoUser",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please write your username";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            Container(
                                padding: EdgeInsets.all(20),
                                child: Center(
                                    child: RaisedButton(
                                        padding: EdgeInsets.all(10),
                                        color: Colors.blue,
                                        child: Text(
                                          "Submit",
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        onPressed: () {
                                          check();
                                        })))
                          ])),
                  Positioned(
                      top: 5.0,
                      right: 5.0,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Login(),
                              ),
                              (Route<dynamic> route) => false,
                            );
                          },
                          child: Align(
                              alignment: Alignment.topRight,
                              child: CircleAvatar(
                                  radius: 20.0,
                                  backgroundColor: Colors.red,
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                  )))))
                ]))));
  }
}
