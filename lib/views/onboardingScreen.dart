import 'package:energy_monitor/views/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final int _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }

    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 30.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Color(0xff5fdde5) : Color(0xff303960),
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 35.0),
                  child: Column(children: <Widget>[
                    Container(
                      alignment: Alignment.centerRight,
                      child: FlatButton(
                        onPressed: () => Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => Login()),
                          (Route<dynamic> route) => false,
                        ),
                        child: Text(
                          'Skip',
                          style: TextStyle(
                            color: Color(0xff0f4c75),
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                    Column(children: <Widget>[
                      Container(
                          height: MediaQuery.of(context).size.height / 1.5,
                          child: PageView(
                              physics: ClampingScrollPhysics(),
                              controller: _pageController,
                              onPageChanged: (int page) {
                                setState(
                                  () {
                                    _currentPage = page;
                                  },
                                );
                              },
                              children: <Widget>[
                                //Screen 1
                                Padding(
                                  padding: EdgeInsets.all(40),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image(
                                          image: AssetImage('images/time.png'),
                                          height: 100,
                                          width: 100,
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 50.0),
                                          child: Text(
                                            "Real Time Monitoring",
                                            style: TextStyle(
                                              color: Color(0xff035aa6),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 25,
                                              height: 1.5,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            'Real time monitoring display of real time recorded data from energy monitoring device that are connected to all the user electronic device',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                //End of Screen 1
                                //-------------------------------------------
                                //Screen 2
                                Padding(
                                  padding: EdgeInsets.all(40),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image(
                                          image: AssetImage(
                                              'images/comparison.png'),
                                          height: 100,
                                          width: 100,
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 50.0),
                                          child: Text(
                                            "Electricity Comparison",
                                            style: TextStyle(
                                              color: Color(0xff035aa6),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 25,
                                              height: 1.5,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            'A feature that compares the difference of electricity consumption of daily, weekly, and monthly electronic device usage',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                //End of Screen 2
                                //---------------------------------------------
                                //Screen 3
                                Padding(
                                  padding: EdgeInsets.all(40),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image(
                                          image: AssetImage(
                                              'images/monthlyBill.png'),
                                          height: 100,
                                          width: 100,
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 50.0),
                                          child: Text(
                                            "Total Electric Bill",
                                            style: TextStyle(
                                              color: Color(0xff035aa6),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 25,
                                              height: 1.5,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            'Displays the total of electric bill based on electricity consumption',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                //End of Screen 3
                                //---------------------------------------------
                                //Screen 3
                                Padding(
                                  padding: EdgeInsets.all(40),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Center(
                                        child: Image(
                                          image: AssetImage(
                                              'images/notification.png'),
                                          height: 100,
                                          width: 100,
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 50.0),
                                          child: Text(
                                            "Notification",
                                            style: TextStyle(
                                              color: Color(0xff035aa6),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 25,
                                              height: 1.5,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 15.0),
                                          child: Text(
                                            'Notification of total daily electricity usage, total electricity bill costs, voltage instability, and the use of electrical power that exceeds its limits',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 15,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                //End of Screen 3
                              ]))
                    ]),
                    Expanded(
                        child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: _buildPageIndicator(),
                            )))
                  ])))),
      bottomSheet: _currentPage == _numPages - 1
          ? Container(
              height: 60.0,
              width: double.infinity,
              color: Color(0xff303960),
              child: GestureDetector(
                  onTap: () => Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => Login()),
                        (Route<dynamic> route) => false,
                      ),
                  child: Center(
                      child: Text('Get started',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          )))))
          : Text(''),
    );
  }
}
