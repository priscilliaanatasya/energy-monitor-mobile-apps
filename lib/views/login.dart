import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/helpCenter.dart';
import 'package:energy_monitor/views/newPass/resetPassword.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'homepage.dart';
import 'package:energy_monitor/css/style.dart' as Style;
import 'package:crypto/crypto.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

var dataUser = new List();

class _LoginState extends State<Login> {
  Timer timer;

  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  bool _secureText = true;
  bool isLogin = false;
  var powerdata;

  String name = "";
  String username = "";
  String email = "";

  showHide() {
    if (this.mounted) {
      setState(() {
        _secureText = !_secureText;
      });
    }
  }

  Future<List> _login() async {
    var url = Help.baseUrlApi() + "/all-user.php";
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var data = jsonDecode(response.body);

    var unameList = new List();
    var passList = new List();
    var nameList = new List();
    var emailList = new List();

    //read user data
    var x = 0;
    while (x < data.length) {
      unameList.add(data[x]["username"]);
      passList.add(data[x]["password_hash"]);
      nameList.add(data[x]["full_name"]);
      emailList.add(data[x]["email"]);
      x++;
    }

    //menampung result dari list
    var result = new List(4);
    result[0] = unameList;
    result[1] = passList;
    result[2] = nameList;
    result[3] = emailList;

    return result;
  }

  submitPower(){
    var url = Help.baseUrlApi() + "/user-power.php";

    http.post(
      url,
      body: {
        "username": _usernameController.text,
      },
    );
  }

  @override
  void initState() {
    timer = new Timer.periodic(
        new Duration(seconds: 2),
        (t) => _login().then((value) {
              dataUser = value;
            }));
    super.initState();
    isLoading = false;
  }

  void showInfoFlushbar(BuildContext context) {
    Flushbar(
      title: "Login Failed!",
      message: "Incorrect Username or Password",
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Colors.red,
      ),
      leftBarIndicatorColor: Colors.red,
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        body: ModalProgressHUD(
            child: _buildBodyWidget(screenSize, context),
            inAsyncCall: isLoading));
  }

  Widget _buildBodyWidget(screenSize, context) {
    return Scaffold(
        body: new Form(
            key: this._formKey,
            child: new Scaffold(
                body: SingleChildScrollView(
                    child: new Column(children: <Widget>[
              new Container(
                padding: const EdgeInsets.all(20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: new Text(
                        "Welcome",
                        style: Style.Default.welcomeText(context),
                      ),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: new Text(
                        "Username",
                        style: Style.Default.loginText(context),
                      ),
                    ),
                    new TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        filled: false,
                        hintText: "Input Your Username Here",
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                      controller: _usernameController,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: new Text(
                        "Password",
                        style: Style.Default.loginText(context),
                      ),
                    ),
                    new TextFormField(
                        obscureText: _secureText,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            filled: false,
                            hintText: "Input Your Password Here",
                            hintStyle: TextStyle(color: Colors.grey),
                            suffixIcon: IconButton(
                              onPressed: showHide,
                              icon: Icon(Icons.visibility),
                            )),
                        controller: _passwordController,
                        style: TextStyle(
                          color: Colors.black,
                        )),
                    new Container(
                      padding: EdgeInsets.only(top: 20.0),
                      width: double.maxFinite,
                      child: new RaisedButton(
                        onPressed: () async {
                          var x = 0;
                          if (dataUser[0].length > x) {
                            while (x < dataUser[0].length) {
                              if (dataUser[0][x] ==
                                      (_usernameController.text) &&
                                  dataUser[1][x] ==
                                      (md5
                                          .convert(utf8
                                              .encode(_passwordController.text))
                                          .toString())) {
                                if (dataUser[0][x] ==
                                    (_usernameController.text)) {
                                  name = dataUser[2][x];
                                  email = dataUser[3][x];
                                }

                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());

                                setState(() {
                                  isLoading = true;
                                });

                                Future.delayed(Duration(seconds: 2), () async {
                                  SharedPreferences pref =
                                      await SharedPreferences.getInstance();
                                  pref.setBool("isLogin", true);
                                  pref.setString(
                                      "username", _usernameController.text);
                                  pref.setString("name", name);
                                  pref.setString("email", email);

                                  setState(() {
                                    isLoading = false;
                                  });
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HomePage()),
                                    (Route<dynamic> route) => false,
                                  );
                                  submitPower();
                                  print(_usernameController.text);
                                  x--;
                                });

                                break;
                              }

                              x++;

                              if (x != -1) {
                                setState(() {
                                  isLoading = true;
                                });
                                Future.delayed(
                                  Duration(seconds: 2),
                                  () async {
                                    SharedPreferences pref =
                                        await SharedPreferences.getInstance();
                                    pref.setBool("isLogin", false);
                                    setState(
                                      () {
                                        showInfoFlushbar(context);
                                        isLoading = false;
                                      },
                                    );
                                  },
                                );
                              }
                            }
                          } else {
                            setState(() {
                              isLoading = true;
                            });
                            Future.delayed(
                              Duration(seconds: 2),
                              () async {
                                SharedPreferences pref =
                                    await SharedPreferences.getInstance();
                                pref.setBool("isLogin", false);
                                setState(
                                  () {
                                    showInfoFlushbar(context);
                                    isLoading = false;
                                  },
                                );
                              },
                            );
                          }
                        },
                        color: Colors.lightBlue,
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20),
                        ),
                        child: Text(
                          "Login",
                          style: Style.Default.loginButton(context),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: new InkWell(
                        child: Text(
                          "Forgot password?",
                          style: Style.Default.loginDetails(context),
                        ),
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                _forgetPass(context),
                          );
                        },
                      ),
                    ),
                    Container(
                      width: 20,
                    ),
                    Container(
                        child: new InkWell(
                            child: Text(
                              "Help Center",
                              style: Style.Default.loginDetails(context),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => new HelpCenter()),
                              );
                            }))
                  ])
            ])))));
  }

  Widget _forgetPass(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      title: Text(
        "Password Reset Request",
        style: Style.Default.forgetPass(context),
      ),
      content: new Text(
        "By clicking the 'Next' button below, you agree to send a password reset request",
        style: Style.Default.forgetDetails(context),
      ),
      actions: <Widget>[
        RaisedButton(
          color: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10),
          ),
          child: Text(
            "Next",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Future.delayed(
              Duration.zero,
              () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => new ResetPassword()),
                  (Route<dynamic> route) => false,
                );
              },
            );
          },
        ),
        RaisedButton(
          color: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10),
          ),
          child: Text(
            "Cancel",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}
