import 'dart:async';
import 'dart:convert';
import 'package:energy_monitor/components/help.dart';
import 'package:energy_monitor/views/appDrawer.dart';
import 'package:energy_monitor/views/feedback/addFeedback.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class FeedbackEmonito extends StatefulWidget {
  @override
  _FeedbackEmonitoState createState() => _FeedbackEmonitoState();
}

class _FeedbackEmonitoState extends State<FeedbackEmonito> {
  List data;
  Timer timer;

  Future<String> getFeedback() async {
    var url = Help.baseUrlApi() + "/list-feedback.php";
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    if (this.mounted) {
      setState(() {
        data = json.decode(response.body);
      });
    }

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => getFeedback());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: new Drawer(
        child: AppDrawer(),
      ),
      appBar: new AppBar(
        title: new Text("Feedback"),
        backgroundColor: Color(0xff0049B8),
        actions: <Widget>[
          new IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => _information(context),
                );
              }),
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        child: data == null
            ? Text(
                "Getting the data...",
                textAlign: TextAlign.center,
                style: Style.Default.getData(context),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Expanded(child: _buildListView()),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddFeedbackEmonito()),
          );
        },
        child: Icon(Icons.add),
        backgroundColor: Color(0xff112f91),
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      itemCount: data == null ? 0 : data.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: _buildRow(
            data[index],
          ),
        );
      },
    );
  }

  Widget _buildRow(item) => Container(
        margin: const EdgeInsets.all(5),
        child: Column(children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              child: Column(children: <Widget>[
                Container(
                    padding: EdgeInsets.fromLTRB(3, 5, 3, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          item['user_feedback'],
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Container(
                          height: 5,
                        ),
                        Text(
                          item['admin_response'],
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Container(height: 10),
                        Container(
                          height: 1,
                          decoration: BoxDecoration(color: Colors.black),
                        ),
                      ],
                    )),
              ]))
        ]),
      );

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Feedback",
        style: Style.Default.infoTitle(context),
      ),
      content: new Text(
        "Menu Feedback adalah menu di mana pengguna dapat mengirimkan pertanyaan atau feedback mengenai sistem pemantauan energi. Pengguna juga dapat melihat pertanyaan atau feedback yang diberikan oleh pengguna lain. Untuk menuliskan pertanyaan atau feedback, pengguna dapat langsung menekan tombol yang berada di sebelah kanan bagian bawah layar",
        style: Style.Default.info(context),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text("Tap anywhere to dismiss",
              style: TextStyle(
                fontSize: 10,
                color: Colors.blue,
              )),
        )
      ],
    );
  }
}
