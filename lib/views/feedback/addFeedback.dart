import 'package:energy_monitor/components/help.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'feedback.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class AddFeedbackEmonito extends StatefulWidget {
  @override
  _AddFeedbackEmonitoState createState() => _AddFeedbackEmonitoState();
}

class _AddFeedbackEmonitoState extends State<AddFeedbackEmonito> {
  TextEditingController _feddbackController = TextEditingController();

  final _key = new GlobalKey<FormState>();

  //save account usename dan name
  String accountUsername = "";
  String accountName = "";

  Future _userData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("username") != null) {
      setState(() {
        accountUsername = pref.getString("username");
        accountName = pref.getString("name");
      });
    } else {
      setState(() {
        accountUsername = "";
        accountName = "";
      });
    }
  }
  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      submit();
    } else {}
  }

  void submit() {
    var url = Help.baseUrlApi() + "/feedback.php";

    http.post(
      url,
      body: {
        "user": accountUsername,
        "user_feedback": _feddbackController.text,
      },
    );
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Thank you!"),
          content: Text("Your feedback / question has been sent!"),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => FeedbackEmonito()),
                  (Route<dynamic> route) => false,
                );
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _userData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _key,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        appBar: new AppBar(
          title: new Text("Send Feedback/Question"),
          backgroundColor: Color(0xff0049B8),
        ),
        body: Container(
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new RichText(
                textAlign: TextAlign.center,
                text: new TextSpan(
                  style: Style.Default.addFeedback(context),
                  children: <TextSpan>[
                    new TextSpan(text: "Sender : "),
                    new TextSpan(text: accountUsername),
                  ],
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(top: 10),
                child: new Text(
                  "Write Your Feedback or Question Below",
                  style: Style.Default.addFeedback(context),
                ),
              ),
              TextFormField(
                maxLines: 5,
                controller: _feddbackController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  filled: false,
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please write any feedback or question";
                  } else {
                    return null;
                  }
                },
              ),
              new Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.all(10),
                child: new RaisedButton(
                  color: Color(0xff0049B8),
                  child: Text(
                    "Send",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    check();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}