import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:async';
import 'package:energy_monitor/components/help.dart';
import 'package:http/http.dart' as http;
import 'package:energy_monitor/css/style.dart' as Style;

class WhatsNew extends StatefulWidget {
  @override
  _WhatsNewState createState() => _WhatsNewState();
}

class _WhatsNewState extends State<WhatsNew> {

  List data;
  Timer timer;

  Future<String> getInfo() async {
    var url = Help.baseUrlApi() + "/list-information.php";
    var response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    if (this.mounted) {
      setState(() {
        data = json.decode(response.body);
      });
    }

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 1), (t) => getInfo());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10),
        child: data == null
            ? Text(
                "Getting the data...",
                textAlign: TextAlign.center,
                style: Style.Default.getData(context),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Expanded(child: _buildListView()),
                ],
              ),
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      itemCount: data == null ? 0 : data.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: _buildRow(
            data[index],
          ),
        );
      },
    );
  }

  Widget _buildRow(item) => Container(
        margin: const EdgeInsets.all(5),
        child: Column(children: <Widget>[
          Container(
              padding: EdgeInsets.all(10),
              child: Column(children: <Widget>[
                Container(
                    padding: EdgeInsets.fromLTRB(3, 10, 3, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          item['information'],
                          style: TextStyle(color: Colors.black),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
              ]))
        ]),
      );
}
