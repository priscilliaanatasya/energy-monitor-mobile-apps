import 'package:energy_monitor/views/appDrawer.dart';
import 'package:energy_monitor/views/information/faq.dart';
import 'package:energy_monitor/views/information/whatsNew.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class Info extends StatefulWidget {
  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info> with SingleTickerProviderStateMixin {
  List<Widget> pages = [
    Faq(),
    WhatsNew(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: new Drawer(
          child: AppDrawer(),
        ),
        appBar: new AppBar(
          title: new Text("Information"),
          backgroundColor: Color(0xff0049B8),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => _information(context),
                  );
                }),
          ],
          bottom: TabBar(
            unselectedLabelColor: Colors.blue,
            labelColor: Colors.yellow,
            indicatorColor: Colors.yellow,
            indicatorWeight: 3.0,
            tabs: <Widget>[
              Tab(text: "FaQ"),
              Tab(text: "Whats New?"),
            ],
          ),
        ),
        body: TabBarView(children: pages),
      ),
    );
  }

  Widget _information(BuildContext context) {
    return new AlertDialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30)),
      backgroundColor: Colors.white,
      title: new Text(
        "Information",
        style: Style.Default.infoTitle(context),
      ),
      content: new Text(
        "Menu information adalah menu dimana pengguna dapat mengetahui informasi seputar alat, aplikasi, atau sistem pemantauan energi. Menu ini terdiri dari 2 (dua) bagian yaitu Frequently Ask Question (Faq) dan What's New yang berisikan informasi terbaru tentang alat, aplikasi, ataupun sistem pemantauan energi",
        style: Style.Default.info(context),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text("Tap anywhere to dismiss",
              style: TextStyle(
                fontSize: 10,
                color: Colors.blue,
              )),
        )
      ],
    );
  }
}
