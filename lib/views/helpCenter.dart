import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:energy_monitor/css/style.dart' as Style;

class HelpCenter extends StatefulWidget {
  @override
  _HelpCenterState createState() => _HelpCenterState();
}

class _HelpCenterState extends State<HelpCenter> {
  void _emailLaunch(String emailId) async {
    var url = "mailto:$emailId?subject = Register a New Account";

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not send E-mail';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Help Center"),
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.all(15),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 20.0),
                        child: Text(
                          "How to create an emonito account?",
                          style: Style.Default.loginText(context),
                        ),
                      ),
                      Text(
                        "To create an account, you can send an email by clicking the button on the bottom right. Don't forget to attach your full name, email, username, address, the number of smart energy monitoring device(s), and electrical power limit at home.",
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 25.0, top: 25.0),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black,
                        ),
                      ),
                      Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Text("Why I can not login?",
                              style: Style.Default.loginText(context),)),
                      Text(
                        "If you have a problem with login, check again your username and password. Also, make sure that your internet connection is stable. If you still have login problem, please contact us by clicking the button on the bottom right.",
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                      Padding(
                          padding:
                              const EdgeInsets.only(bottom: 25.0, top: 25.0),
                          child: Container(
                            height: 1.0,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.black,
                          ))
                    ]))),
        floatingActionButton: FloatingActionButton.extended(
            icon: Icon(Icons.email),
            label: Text(
              "Send an email",
            ),
            onPressed: () {
              _emailLaunch('emonitocontact@gmail.com');
            }));
  }
}
